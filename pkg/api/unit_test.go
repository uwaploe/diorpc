package api

import (
	"testing"

	"github.com/BurntSushi/toml"
)

var CFG = `
[[config]]
line = 1
dir = "OUTPUT"
pol = "NORMAL"
[[config]]
line = 6
dir = "INPUT"
[[config]]
line = 8
dir = "OUTPUT"
pol = "INVERTED"
`

type Setup struct {
	Config []LineCfg
}

func TestUnmarshalLineCfg(t *testing.T) {
	var setup Setup
	_, err := toml.Decode(CFG, &setup)
	if err != nil {
		t.Fatal(err)
	}

	if n := len(setup.Config); n != 3 {
		t.Errorf("Array decode failed; expected 3 entries, got %d", n)
	}
}

func TestUnmarshalDioSetup(t *testing.T) {
	var setup DioSetup
	_, err := toml.Decode(CFG, &setup)
	if err != nil {
		t.Fatal(err)
	}

	if n := len(setup.Cfg); n != 3 {
		t.Errorf("Array decode failed; expected 3 entries, got %d", n)
	}

	if setup.Cfg[1].Line != 6 {
		t.Errorf("Entry decode error; %#v", *setup.Cfg[1])
	}
}
