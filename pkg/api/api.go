package api

import fmt "fmt"

//go:generate protoc -I. --go_out=plugins=grpc:. api.proto

func (l *LineCfg) UnmarshalTOML(data interface{}) error {
	d, ok := data.(map[string]interface{})
	if !ok {
		return fmt.Errorf("Cannot convert from: %t", data)
	}

	l.Line = int32(d["line"].(int64))
	if v, ok := d["dir"]; ok {
		l.Dir = Direction(Direction_value[v.(string)])
	}
	if v, ok := d["pol"]; ok {
		l.Pol = Polarity(Polarity_value[v.(string)])
	}

	return nil
}

func (m *DioSetup) UnmarshalTOML(data interface{}) error {
	d, ok := data.(map[string]interface{})
	if !ok {
		return fmt.Errorf("Cannot convert from: %t", data)
	}
	config, _ := d["config"].([]map[string]interface{})
	m.Cfg = make([]*LineCfg, 0, len(config))
	for _, cfg := range config {
		lcfg := LineCfg{}
		lcfg.UnmarshalTOML(cfg)
		m.Cfg = append(m.Cfg, &lcfg)
	}

	return nil
}
