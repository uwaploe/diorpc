package main

import (
	"context"
	"io"
	"log"
	"os"
	"sync"

	"bitbucket.org/uwaploe/diorpc/pkg/api"
	"github.com/BurntSushi/toml"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	IsaOffset int64 = 0xc80
	DirReg          = 0x14
	PolReg          = 0x16
	OutReg          = 0x18
	InReg           = 0x1a
)

const MaxLines int = 16

type ReadWriterAt interface {
	io.ReaderAt
	io.WriterAt
}

type dioServer struct {
	fp     ReadWriterAt
	offset int64
	mu     *sync.Mutex
	debug  bool
}

func (s *dioServer) modifyReg(reg int64, value, mask uint8) (uint8, error) {
	var b [1]byte

	s.mu.Lock()
	defer s.mu.Unlock()

	offset := s.offset + reg
	_, err := s.fp.ReadAt(b[:], offset)
	if err != nil {
		return 0, err
	}

	if s.debug {
		log.Printf("Read @%#x: %#02x", offset, b[0])
	}

	b[0] = (b[0] &^ mask) | (value & mask)
	_, err = s.fp.WriteAt(b[:], offset)

	if s.debug {
		log.Printf("Write @%#x: %#02x", offset, b[0])
	}

	return b[0], err
}

func (s *dioServer) modifyWord(reg int64, value, mask uint16) (uint16, error) {
	var (
		low, high uint8
		err       error
	)

	low = uint8(value & 0xff)
	high = uint8((value >> 8) & 0xff)

	low, err = s.modifyReg(reg, low, uint8(mask&0xff))
	if err != nil {
		return 0, err
	}
	high, err = s.modifyReg(reg+1, high, uint8((mask>>8)&0xff))

	return uint16(high)<<8 | uint16(low), err
}

func (s *dioServer) readWord(reg int64) (uint16, error) {
	var (
		b         [1]byte
		low, high uint16
	)

	s.mu.Lock()
	defer s.mu.Unlock()

	offset := s.offset + reg
	_, err := s.fp.ReadAt(b[:], offset)
	if err != nil {
		return 0, err
	}
	low = uint16(b[0])
	_, err = s.fp.ReadAt(b[:], offset+1)
	if err != nil {
		return 0, err
	}
	high = uint16(b[0]) << 8

	return high | low, nil
}

func (s *dioServer) setLines(bits uint16) (uint16, error) {
	return s.modifyWord(OutReg, bits, bits)
}

func (s *dioServer) clearLines(bits uint16) (uint16, error) {
	return s.modifyWord(OutReg, 0, bits)
}

func (s *dioServer) readLines() (uint16, error) {
	return s.readWord(InReg)
}

func (s *dioServer) initialSetup(file string) (*api.DioSetup, error) {
	setup := api.DioSetup{}
	_, err := toml.DecodeFile(file, &setup)
	if err != nil {
		return nil, err
	}

	return s.Config(context.Background(), &setup)
}

func genDioSetup(dir, pol uint16) api.DioSetup {
	setup := api.DioSetup{}
	setup.Cfg = make([]*api.LineCfg, 0)

	mask := uint16(1)
	for i := 0; i < 16; i++ {
		cfg := api.LineCfg{}
		cfg.Line = int32(i)
		if (dir & mask) == mask {
			cfg.Dir = api.Direction_OUTPUT
		}
		if (pol & mask) == mask {
			cfg.Pol = api.Polarity_INVERTED
		}
		setup.Cfg = append(setup.Cfg, &cfg)
		mask = mask << 1
	}

	return setup
}

func (s *dioServer) Config(ctx context.Context, cfgs *api.DioSetup) (*api.DioSetup, error) {
	var (
		dir, pol, mask, bit uint16
	)

	for _, cfg := range cfgs.Cfg {
		bit = 1 << int(cfg.GetLine())
		mask = mask | bit
		if cfg.GetDir() == api.Direction_OUTPUT {
			dir = dir | bit
		}
		if cfg.GetPol() == api.Polarity_INVERTED {
			pol = pol | bit
		}
	}

	// Set the direction register
	dir, err := s.modifyWord(DirReg, dir, mask)
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	// Set the polarity register
	pol, err = s.modifyWord(PolReg, pol, mask)
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}

	resp := genDioSetup(dir, pol)

	return &resp, nil
}

func (s *dioServer) Set(ctx context.Context, lines *api.DioLines) (*api.DioState, error) {
	var (
		output uint16
		state  api.DioState
	)

	for _, line := range lines.Line {
		if line < 0 || int(line) >= MaxLines {
			return nil, status.Errorf(codes.InvalidArgument, "Line out of range: %d", line)
		}
		output = output | (1 << int(line))
	}

	value, err := s.setLines(output)
	if err != nil {
		return &state, status.Error(codes.Internal, err.Error())
	}

	state.Value = int32(value)
	return &state, nil
}

func (s *dioServer) Test(ctx context.Context, _ *api.Empty) (*api.DioState, error) {
	var state api.DioState

	value, err := s.readLines()
	state.Value = int32(value)

	return &state, err
}

func (s *dioServer) Clear(ctx context.Context, lines *api.DioLines) (*api.DioState, error) {
	var (
		output uint16
		state  api.DioState
	)

	for _, line := range lines.Line {
		if line < 0 || int(line) >= MaxLines {
			return nil, status.Errorf(codes.InvalidArgument, "Line out of range: %d", line)
		}
		output = output | (1 << int(line))
	}

	value, err := s.clearLines(output)
	if err != nil {
		return &state, status.Error(codes.Internal, err.Error())
	}

	state.Value = int32(value)
	return &state, nil
}

func newDioServer(offset int64, debug bool) *dioServer {
	f, err := os.OpenFile("/dev/port", os.O_RDWR|os.O_SYNC, 0660)
	if err != nil {
		return nil
	}
	return &dioServer{
		fp:     f,
		offset: offset,
		mu:     &sync.Mutex{},
		debug:  debug,
	}
}
