// gRPC server to manage the 16 DIO lines on a Versalogic VL-EPMe-30 (Bengal)
// CPU board.
package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	"bitbucket.org/uwaploe/diorpc/pkg/api"
	"github.com/coreos/go-systemd/v22/activation"
	"google.golang.org/grpc"
)

var Usage = `Usage: dioserver [options]

Grpc service to manage the DIO lines on a Versalogic VL-EPMe-30 CPU board.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	svcAddr   string = ":10101"
	cfgFile   string
	debugMode bool
)

func lookupEnvOrString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

func envIsSet(key string) bool {
	_, ok := os.LookupEnv(key)
	return ok
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&svcAddr, "addr",
		lookupEnvOrString("DIOSVC_ADDR", svcAddr), "Server listen address")
	flag.BoolVar(&debugMode, "debug", envIsSet("DIOSVC_DEBUG"), "If true, enable debug output")
	flag.StringVar(&cfgFile, "config",
		lookupEnvOrString("DIOSVC_CONFIG", cfgFile), "Initial DIO configuration")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	listeners, err := activation.Listeners()
	if err != nil {
		log.Fatalf("Systemd activation error: %v", err)
	}

	var listener net.Listener
	switch n := len(listeners); n {
	case 0:
		// Not activated by a Systemd socket ...
		addr, _ := net.ResolveTCPAddr("tcp", svcAddr)
		listener, err = net.ListenTCP("tcp", addr)
		if err != nil {
			log.Fatal(err)
		}
	case 1:
		listener = listeners[0]
	default:
		log.Fatalf("Unexpected number of socket activation fds: %d", n)
	}

	svc := newDioServer(IsaOffset, debugMode)
	if cfgFile != "" {
		setup, err := svc.initialSetup(cfgFile)
		if err != nil {
			log.Printf("Initial setup failed: %v", err)
		} else {
			log.Printf("Initial setup: %q", setup)
		}
	}

	opts := make([]grpc.ServerOption, 0)
	grpcServer := grpc.NewServer(opts...)
	api.RegisterDioServiceServer(grpcServer, svc)

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s := <-sigs
		log.Printf("Got signal: %v", s)
		grpcServer.GracefulStop()
	}()

	log.Printf("DIO gRPC Server %s starting", Version)
	grpcServer.Serve(listener)
}
