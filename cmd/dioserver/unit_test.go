package main

import (
	"context"
	"encoding/json"
	"net"
	"sync"
	"testing"

	"bitbucket.org/uwaploe/diorpc/pkg/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type rwbuffer struct {
	buf     [64]byte
	verbose bool
}

func (rw *rwbuffer) ReadAt(p []byte, offset int64) (n int, err error) {
	n = len(p)

	for i := 0; i < n; i++ {
		p[i] = rw.buf[offset+int64(i)]
	}

	return
}

func (rw *rwbuffer) WriteAt(p []byte, offset int64) (n int, err error) {
	n = len(p)
	for i := 0; i < n; i++ {
		rw.buf[offset+int64(i)] = p[i]
	}

	return
}

func (rw *rwbuffer) GetByte(i int) byte {
	return rw.buf[i]
}

func (rw *rwbuffer) GetWord(i int) uint16 {
	return uint16(rw.buf[i]) | (uint16(rw.buf[i+1]) << 8)
}

func TestBitSet(t *testing.T) {
	rw := &rwbuffer{}
	s := &dioServer{
		fp:     rw,
		offset: 0,
		mu:     &sync.Mutex{},
		debug:  testing.Verbose(),
	}

	_, err := s.setLines(0x5aa5)
	if err != nil {
		t.Fatal(err)
	}

	if rw.GetByte(OutReg) != 0xa5 {
		t.Errorf("Bad value; expected a5, got %02x", rw.GetByte(OutReg))
	}
	if rw.GetByte(OutReg+1) != 0x5a {
		t.Errorf("Bad value; expected 5a, got %02x", rw.GetByte(OutReg+1))
	}
}

func testSet(t *testing.T, client api.DioServiceClient) {
	table := []struct {
		in   api.DioLines
		out  uint16
		code codes.Code
	}{
		{
			in:   api.DioLines{Line: []int32{0, 2, 4, 6, 8, 10, 12, 14}},
			out:  0x5555,
			code: codes.OK,
		},
		{
			in:   api.DioLines{Line: []int32{1, 3, 5, 7, 9, 11, 13, 15}},
			out:  0xaaaa,
			code: codes.OK,
		},
		{
			in:   api.DioLines{Line: []int32{16}},
			code: codes.InvalidArgument,
		},
	}

	for _, e := range table {
		state, err := client.Set(context.Background(), &e.in)
		if err != nil {
			stat := status.Convert(err)
			if stat.Code() != e.code {
				t.Errorf("Unexpected error status: %#v", stat.Code())
			}
		} else {
			if uint16(state.Value) != e.out {
				t.Errorf("Bad return value; expected %04x, got %04x", e.out, state.Value)
			}
			_, err = client.Clear(context.Background(), &e.in)
			if err != nil {
				t.Error(err)
			}
		}
	}
}

func matchLineCfg(a, b *api.LineCfg) bool {
	s1, _ := json.Marshal(*a)
	s2, _ := json.Marshal(*b)
	return len(s1) > 0 && string(s1) == string(s2)
}

func testConfig(t *testing.T, client api.DioServiceClient) {
	table := []struct {
		cfg      api.DioSetup
		dir, pol uint16
	}{
		{
			cfg: api.DioSetup{Cfg: []*api.LineCfg{&api.LineCfg{Line: 1, Dir: 1}}},
			dir: 0x0002,
			pol: 0,
		},
		{
			cfg: api.DioSetup{Cfg: []*api.LineCfg{&api.LineCfg{Line: 9, Dir: 1}}},
			dir: 0x0202,
			pol: 0,
		},
		{
			cfg: api.DioSetup{Cfg: []*api.LineCfg{&api.LineCfg{Line: 1, Dir: 0}}},
			dir: 0x0200,
			pol: 0,
		},
		{
			cfg: api.DioSetup{Cfg: []*api.LineCfg{
				&api.LineCfg{Line: 1, Dir: 1, Pol: 1},
				&api.LineCfg{Line: 9, Dir: 1}}},
			dir: 0x0202,
			pol: 0x0002,
		},
		{
			cfg: api.DioSetup{Cfg: []*api.LineCfg{
				&api.LineCfg{Line: 1, Dir: 1},
				&api.LineCfg{Line: 9, Dir: 0}}},
			dir: 0x0002,
			pol: 0,
		},
	}

	for _, e := range table {
		cfgs, err := client.Config(context.Background(), &e.cfg)
		if err != nil {
			t.Error(err)
		}
		expected := genDioSetup(e.dir, e.pol)

		for _, lc := range expected.Cfg {
			if !matchLineCfg(lc, cfgs.Cfg[lc.Line]) {
				t.Errorf("Config mismatch; %#v != %#v",
					*lc, *cfgs.Cfg[lc.Line])
			}
		}
	}
}

func testRead(t *testing.T, client api.DioServiceClient, val uint16) {
	resp, err := client.Test(context.Background(), &api.Empty{})
	if err != nil {
		t.Error(err)
	}
	if uint16(resp.Value) != val {
		t.Errorf("Bad value; expected %#04x, got %#04x", val, resp.Value)
	}
}

func startTestServer(t *testing.T, inval uint16) (*grpc.Server, string) {
	rw := &rwbuffer{}
	rw.buf[InReg] = uint8(inval & 0xff)
	rw.buf[InReg+1] = uint8((inval >> 8) & 0xff)
	s := &dioServer{
		fp:     rw,
		offset: 0,
		mu:     &sync.Mutex{},
	}
	gsvr := grpc.NewServer()
	api.RegisterDioServiceServer(gsvr, s)
	listener, err := net.Listen("tcp", ":0")
	if err != nil {
		t.Fatal(err)
	}

	go gsvr.Serve(listener)

	return gsvr, listener.Addr().String()
}

func newClient(t *testing.T, addr string) api.DioServiceClient {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		t.Fatal(err)
	}
	return api.NewDioServiceClient(conn)
}

func TestServer(t *testing.T) {
	gsvr, addr := startTestServer(t, 0x4242)
	defer gsvr.Stop()
	client := newClient(t, addr)

	t.Run("TestConfig", func(t *testing.T) { testConfig(t, client) })
	t.Run("TestSet", func(t *testing.T) { testSet(t, client) })
	t.Run("TestRead", func(t *testing.T) { testRead(t, client, 0x4242) })

}
