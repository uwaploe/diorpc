// Tbenable controls the hardware enable line for the inVADER timing board.
package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"strings"
	"time"

	"bitbucket.org/uwaploe/diorpc/pkg/api"
	"google.golang.org/grpc"
)

var Usage = `Usage: tbenable [options] on|off

Switch the timing Board enable line on or off.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	svcAddr string        = "localhost:10101"
	timeout time.Duration = time.Second * 4
	dioLine int           = 0
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&svcAddr, "addr", svcAddr, "DIO server address")
	flag.DurationVar(&timeout, "timeout", timeout, "maximum time for server response")
	flag.IntVar(&dioLine, "dio", dioLine, "DIO line to use")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func newClient(addr string) api.DioServiceClient {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	return api.NewDioServiceClient(conn)
}

func main() {
	args := parseCmdLine()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	var (
		err  error
		resp *api.DioState
	)

	client := newClient(svcAddr)

	ctx, cancel := context.WithTimeout(context.Background(), timeout)

	switch strings.ToLower(args[0]) {
	case "on", "yes", "true", "1":
		resp, err = client.Set(ctx, &api.DioLines{Line: []int32{int32(dioLine)}})
	case "off", "no", "false", "0":
		resp, err = client.Clear(ctx, &api.DioLines{Line: []int32{int32(dioLine)}})
	default:
		log.Printf("Invalid operation: %q", args[0])
	}
	cancel()

	if err != nil {
		log.Fatalf("Operation failed: %v", err)
	}
	log.Printf("DIO state: %q", resp)
}
