module bitbucket.org/uwaploe/diorpc

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/coreos/go-systemd/v22 v22.0.0
	github.com/golang/protobuf v1.3.2
	google.golang.org/grpc v1.26.0

)
